
PROCEDURE AddTextEnt (ent : IN OUT entity; atrname : string);   EXTERNAL;

PROCEDURE LabelCntr (Label : IN OUT array of entity;
										 NumLines : integer;
										 Cntr : IN OUT point);  EXTERNAL;
										 
PROCEDURE GetLblOutline (Label : IN OUT array of entity; NumLines : integer; 
												 outline : IN array of point; cnt : out integer); EXTERNAL;

FUNCTION LabelFits (pline : IN entity; Label : array of entity; NumLines : integer) : boolean;   EXTERNAL;
FUNCTION RotLabelFits (pline : IN entity; Label : array of entity; NumLines : integer) : boolean;   EXTERNAL;
