PROCEDURE UpdateAll;			EXTERNAL;

PROCEDURE UpdateOne (ent : IN OUT entity);			EXTERNAL;

PROCEDURE ReplaceTags (str : IN OUT str255; s_num, s_name, s_area, s_hdim, s_vdim, s_perim, s_parea, s_vol, s_percent, s_lyr, s_cat, s_count : IN string; hdimsmall : boolean); EXTERNAL;

PROCEDURE AddMeasures (ent : IN OUT entity; existing : boolean); EXTERNAL;

PROCEDURE GetLinePat (ndx : integer; dest : IN OUT string); EXTERNAL;

PROCEDURE NumberPrompt (NewCopyNum : string); EXTERNAL;