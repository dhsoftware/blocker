
FUNCTION GetTxtStyle (ndx : integer; 
											Styles : IN OUT array of TextStyle;
											inputmode : integer;
											pnt : IN OUT point;
											key : IN OUT integer;
											CADVersion : integer) : integer; EXTERNAL;

PROCEDURE FormatTxt (ent : IN OUT entity; Style : TextStyle); EXTERNAL;

PROCEDURE TxtSzSaveName (str : IN OUT string; ndx : integer);  EXTERNAL;
