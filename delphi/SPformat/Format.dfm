object fmMain: TfmMain
  Left = 0
  Top = 0
  Caption = 'Report Layout'
  ClientHeight = 352
  ClientWidth = 727
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lblMsg: TLabel
    Left = 16
    Top = 298
    Width = 3
    Height = 13
  end
  object btnNbr: TButton
    Left = 16
    Top = 18
    Width = 75
    Height = 17
    Hint = 'Room Number'
    Caption = '{nbr}'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
    TabStop = False
    OnClick = btnNbrClick
  end
  object btnName: TButton
    Left = 16
    Top = 34
    Width = 75
    Height = 17
    Hint = 'Room Name'
    Caption = '{name}'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    TabStop = False
    OnClick = btnNameClick
  end
  object btnHDim: TButton
    Left = 16
    Top = 50
    Width = 75
    Height = 17
    Hint = 'Horizontal Dimension of Room'
    Caption = '{hdim}'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 2
    TabStop = False
    OnClick = btnHDimClick
  end
  object btnVDim: TButton
    Left = 16
    Top = 66
    Width = 75
    Height = 17
    Hint = 'Vertical Dimension of Room'
    Caption = '{vdim}'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 3
    TabStop = False
    OnClick = btnVDimClick
  end
  object btnSDim: TButton
    Left = 16
    Top = 82
    Width = 75
    Height = 17
    Hint = 'Smaller of the 2 room dimensions (either horizontal or vertical)'
    Caption = '{sdim}'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 4
    TabStop = False
    OnClick = btnSDimClick
  end
  object btnLDim: TButton
    Left = 16
    Top = 98
    Width = 75
    Height = 17
    Hint = 'Larger of the 2 room dimensions (either horizontal or vertical)'
    Caption = '{ldim}'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 5
    TabStop = False
    OnClick = btnLDimClick
  end
  object btnArea: TButton
    Left = 16
    Top = 146
    Width = 75
    Height = 17
    Hint = 'Room Area'
    Caption = '{area}'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 6
    TabStop = False
    OnClick = btnAreaClick
  end
  object btnAUnit: TButton
    Left = 16
    Top = 162
    Width = 75
    Height = 17
    Hint = 'Room Area Units'
    Caption = '{aunit}'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 7
    TabStop = False
    OnClick = btnAUnitClick
  end
  object BtnDUnit: TButton
    Left = 16
    Top = 130
    Width = 75
    Height = 17
    Hint = 'Dimension Units'
    Caption = '{dunit}'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 8
    TabStop = False
    OnClick = BtnDUnitClick
  end
  object btnPerim: TButton
    Left = 16
    Top = 114
    Width = 75
    Height = 17
    Hint = 'Perimeter'
    Caption = '{perim}'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 9
    TabStop = False
    OnClick = btnPerimClick
  end
  object btnPArea: TButton
    Left = 16
    Top = 178
    Width = 75
    Height = 17
    Hint = 'Perimeter Area'
    Caption = '{parea}'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 10
    TabStop = False
    OnClick = btnPAreaClick
  end
  object btnPAUnit: TButton
    Left = 16
    Top = 194
    Width = 75
    Height = 17
    Hint = 'Perimeter Area Units'
    Caption = '{paunit}'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 11
    TabStop = False
    OnClick = btnPAUnitClick
  end
  object btnVol: TButton
    Left = 16
    Top = 210
    Width = 75
    Height = 17
    Hint = 'Volume of Room'
    Caption = '{vol}'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 12
    TabStop = False
    OnClick = btnVolClick
  end
  object btnVUnits: TButton
    Left = 16
    Top = 226
    Width = 75
    Height = 17
    Hint = 'Volume Units'
    Caption = '{vunit}'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 13
    TabStop = False
    OnClick = btnVUnitsClick
  end
  object btnPct: TButton
    Left = 16
    Top = 242
    Width = 75
    Height = 17
    Hint = 'Area percentage of total'
    Caption = '{area%}'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 14
    TabStop = False
    OnClick = btnPctClick
  end
  object Panel1: TPanel
    Left = 126
    Top = 76
    Width = 593
    Height = 219
    BevelOuter = bvNone
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 15
    object LblHeadings: TLabel
      Left = 0
      Top = 67
      Width = 83
      Height = 14
      Caption = 'Col Headings:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LblData: TLabel
      Left = 0
      Top = 121
      Width = 62
      Height = 14
      Caption = 'Data Line:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblSubTots: TLabel
      Left = 0
      Top = 150
      Width = 70
      Height = 14
      Caption = 'Sub-Totals:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LblTots: TLabel
      Left = 0
      Top = 175
      Width = 41
      Height = 14
      Caption = 'Totals:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LblTitle: TLabel
      Left = 0
      Top = 10
      Width = 77
      Height = 14
      Caption = 'Report Title:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LblSubHeads: TLabel
      Left = 0
      Top = 94
      Width = 83
      Height = 14
      Caption = 'Sub-Heading:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object ScrollBox1: TScrollBox
      Left = 88
      Top = 0
      Width = 505
      Height = 219
      HorzScrollBar.Position = 398
      Align = alRight
      BevelEdges = []
      BevelInner = bvNone
      BevelOuter = bvNone
      BorderStyle = bsNone
      TabOrder = 0
      object Shape1: TShape
        Left = -395
        Top = 37
        Width = 900
        Height = 21
        Brush.Color = clBtnFace
        Pen.Width = 0
      end
      object lblCol1: TLabel
        Left = -368
        Top = 42
        Width = 44
        Height = 13
        Caption = 'Column 1'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lblCol2: TLabel
        Left = -278
        Top = 42
        Width = 44
        Height = 13
        Caption = 'Column 2'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lblCol3: TLabel
        Left = -188
        Top = 42
        Width = 44
        Height = 13
        Caption = 'Column 3'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lblCol4: TLabel
        Left = -98
        Top = 42
        Width = 44
        Height = 13
        Caption = 'Column 4'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lblCol5: TLabel
        Left = -8
        Top = 42
        Width = 44
        Height = 13
        Caption = 'Column 5'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lblCol6: TLabel
        Left = 82
        Top = 42
        Width = 44
        Height = 13
        Caption = 'Column 6'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lblCol7: TLabel
        Left = 172
        Top = 42
        Width = 44
        Height = 13
        Caption = 'Column 7'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lblCol8: TLabel
        Left = 262
        Top = 42
        Width = 44
        Height = 13
        Caption = 'Column 8'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lblCol9: TLabel
        Left = 352
        Top = 42
        Width = 44
        Height = 13
        Caption = 'Column 9'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lblCol10: TLabel
        Left = 442
        Top = 42
        Width = 50
        Height = 13
        Caption = 'Column 10'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object edHead1: TEdit
        Left = -395
        Top = 64
        Width = 88
        Height = 19
        MaxLength = 25
        TabOrder = 1
        OnEnter = edHeadEnter
      end
      object edHead2: TEdit
        Left = -305
        Top = 64
        Width = 88
        Height = 19
        MaxLength = 25
        TabOrder = 2
        OnEnter = edHeadEnter
      end
      object edHead8: TEdit
        Left = 235
        Top = 64
        Width = 88
        Height = 19
        MaxLength = 25
        TabOrder = 8
        OnEnter = edHeadEnter
      end
      object edHead7: TEdit
        Left = 145
        Top = 64
        Width = 88
        Height = 19
        MaxLength = 25
        TabOrder = 7
        OnEnter = edHeadEnter
      end
      object edHead6: TEdit
        Left = 55
        Top = 64
        Width = 88
        Height = 19
        MaxLength = 25
        TabOrder = 6
        OnEnter = edHeadEnter
      end
      object edHead3: TEdit
        Left = -215
        Top = 64
        Width = 88
        Height = 19
        MaxLength = 25
        TabOrder = 3
        OnEnter = edHeadEnter
      end
      object edHead4: TEdit
        Left = -125
        Top = 64
        Width = 88
        Height = 19
        MaxLength = 25
        TabOrder = 4
        OnEnter = edHeadEnter
      end
      object edHead5: TEdit
        Left = -35
        Top = 64
        Width = 88
        Height = 19
        MaxLength = 25
        TabOrder = 5
        OnEnter = edHeadEnter
      end
      object edHead9: TEdit
        Left = 325
        Top = 64
        Width = 88
        Height = 19
        MaxLength = 25
        TabOrder = 9
        OnEnter = edHeadEnter
      end
      object edHead10: TEdit
        Left = 415
        Top = 64
        Width = 88
        Height = 19
        MaxLength = 25
        TabOrder = 10
        OnEnter = edHeadEnter
      end
      object edData1: TEdit
        Left = -395
        Top = 118
        Width = 88
        Height = 19
        MaxLength = 35
        TabOrder = 12
        OnEnter = edEnter
        OnExit = edExit
      end
      object edData9: TEdit
        Left = 325
        Top = 118
        Width = 88
        Height = 19
        MaxLength = 35
        TabOrder = 20
        OnEnter = edEnter
        OnExit = edExit
      end
      object edData8: TEdit
        Left = 235
        Top = 118
        Width = 88
        Height = 19
        MaxLength = 35
        TabOrder = 19
        OnEnter = edEnter
        OnExit = edExit
      end
      object edData7: TEdit
        Left = 145
        Top = 118
        Width = 88
        Height = 19
        MaxLength = 35
        TabOrder = 18
        OnEnter = edEnter
        OnExit = edExit
      end
      object edData6: TEdit
        Left = 55
        Top = 118
        Width = 88
        Height = 19
        MaxLength = 35
        TabOrder = 17
        OnEnter = edEnter
        OnExit = edExit
      end
      object edData2: TEdit
        Left = -305
        Top = 118
        Width = 88
        Height = 19
        MaxLength = 35
        TabOrder = 13
        OnEnter = edEnter
        OnExit = edExit
      end
      object edData3: TEdit
        Left = -215
        Top = 118
        Width = 88
        Height = 19
        MaxLength = 35
        TabOrder = 14
        OnEnter = edEnter
        OnExit = edExit
      end
      object edData4: TEdit
        Left = -125
        Top = 118
        Width = 88
        Height = 19
        MaxLength = 35
        TabOrder = 15
        OnEnter = edEnter
        OnExit = edExit
      end
      object edData5: TEdit
        Left = -35
        Top = 118
        Width = 88
        Height = 19
        MaxLength = 35
        TabOrder = 16
        OnEnter = edEnter
        OnExit = edExit
      end
      object edData10: TEdit
        Left = 415
        Top = 118
        Width = 88
        Height = 19
        MaxLength = 35
        TabOrder = 21
        OnEnter = edEnter
        OnExit = edExit
      end
      object edSubTot1: TEdit
        Left = -395
        Top = 145
        Width = 88
        Height = 19
        MaxLength = 35
        TabOrder = 22
        OnEnter = edEnter
        OnExit = edExit
      end
      object edSubTot2: TEdit
        Left = -305
        Top = 145
        Width = 88
        Height = 19
        MaxLength = 35
        TabOrder = 23
        OnEnter = edEnter
        OnExit = edExit
      end
      object edSubTot3: TEdit
        Left = -215
        Top = 145
        Width = 88
        Height = 19
        MaxLength = 35
        TabOrder = 24
        OnEnter = edEnter
        OnExit = edExit
      end
      object edSubTot4: TEdit
        Left = -125
        Top = 145
        Width = 88
        Height = 19
        MaxLength = 35
        TabOrder = 25
        OnEnter = edEnter
        OnExit = edExit
      end
      object edSubTot5: TEdit
        Left = -35
        Top = 145
        Width = 88
        Height = 19
        MaxLength = 35
        TabOrder = 26
        OnEnter = edEnter
        OnExit = edExit
      end
      object edSubTot6: TEdit
        Left = 55
        Top = 145
        Width = 88
        Height = 19
        MaxLength = 35
        TabOrder = 27
        OnEnter = edEnter
        OnExit = edExit
      end
      object edSubTot7: TEdit
        Left = 145
        Top = 145
        Width = 88
        Height = 19
        MaxLength = 35
        TabOrder = 28
        OnEnter = edEnter
        OnExit = edExit
      end
      object edSubTot8: TEdit
        Left = 235
        Top = 145
        Width = 88
        Height = 19
        MaxLength = 35
        TabOrder = 29
        OnEnter = edEnter
        OnExit = edExit
      end
      object edSubTot9: TEdit
        Left = 325
        Top = 145
        Width = 88
        Height = 19
        MaxLength = 35
        TabOrder = 30
        OnEnter = edEnter
        OnExit = edExit
      end
      object edSubTot10: TEdit
        Left = 415
        Top = 145
        Width = 88
        Height = 19
        MaxLength = 35
        TabOrder = 31
        OnEnter = edEnter
        OnExit = edExit
      end
      object EdTot1: TEdit
        Left = -395
        Top = 172
        Width = 88
        Height = 19
        MaxLength = 35
        TabOrder = 32
        OnEnter = edEnter
        OnExit = edExit
      end
      object EdTot2: TEdit
        Left = -305
        Top = 172
        Width = 88
        Height = 19
        MaxLength = 35
        TabOrder = 33
        OnEnter = edEnter
        OnExit = edExit
      end
      object EdTot3: TEdit
        Left = -215
        Top = 172
        Width = 88
        Height = 19
        MaxLength = 35
        TabOrder = 34
        OnEnter = edEnter
        OnExit = edExit
      end
      object EdTot4: TEdit
        Left = -125
        Top = 172
        Width = 88
        Height = 19
        MaxLength = 35
        TabOrder = 35
        OnEnter = edEnter
        OnExit = edExit
      end
      object EdTot5: TEdit
        Left = -35
        Top = 172
        Width = 88
        Height = 19
        MaxLength = 35
        TabOrder = 36
        OnEnter = edEnter
        OnExit = edExit
      end
      object EdTot6: TEdit
        Left = 55
        Top = 172
        Width = 88
        Height = 19
        MaxLength = 35
        TabOrder = 37
        OnEnter = edEnter
        OnExit = edExit
      end
      object EdTot7: TEdit
        Left = 145
        Top = 172
        Width = 88
        Height = 19
        MaxLength = 35
        TabOrder = 38
        OnEnter = edEnter
        OnExit = edExit
      end
      object EdTot8: TEdit
        Left = 235
        Top = 172
        Width = 88
        Height = 19
        MaxLength = 35
        TabOrder = 39
        OnEnter = edEnter
        OnExit = edExit
      end
      object EdTot9: TEdit
        Left = 325
        Top = 172
        Width = 88
        Height = 19
        MaxLength = 35
        TabOrder = 40
        OnEnter = edEnter
        OnExit = edExit
      end
      object EdTot10: TEdit
        Left = 415
        Top = 172
        Width = 88
        Height = 19
        MaxLength = 35
        TabOrder = 41
        OnEnter = edEnter
        OnExit = edExit
      end
      object EdTitle: TEdit
        Left = -395
        Top = 7
        Width = 358
        Height = 19
        MaxLength = 50
        TabOrder = 0
        OnEnter = edHeadEnter
      end
      object EdSubHdg: TEdit
        Left = -395
        Top = 91
        Width = 358
        Height = 19
        MaxLength = 50
        TabOrder = 11
        OnEnter = edEnter
        OnExit = edExit
      end
    end
  end
  object memInstructions: TMemo
    Left = 127
    Top = 17
    Width = 578
    Height = 56
    TabStop = False
    BorderStyle = bsNone
    Color = clBtnFace
    Ctl3D = False
    ParentCtl3D = False
    ReadOnly = True
    TabOrder = 16
  end
  object btnSave: TButton
    Left = 581
    Top = 319
    Width = 136
    Height = 25
    Caption = 'Save'
    TabOrder = 17
    OnClick = btnSaveClick
    OnEnter = edHeadEnter
  end
  object btnCancel: TButton
    Left = 472
    Top = 319
    Width = 75
    Height = 25
    Caption = 'Cancel'
    TabOrder = 18
    OnClick = btnCancelClick
    OnEnter = edHeadEnter
  end
  object BtnCat: TButton
    Left = 16
    Top = 258
    Width = 75
    Height = 17
    Hint = 'Category'
    Caption = '{cat}'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 19
    TabStop = False
    OnClick = BtnCatClick
  end
  object BtnLyr: TButton
    Left = 16
    Top = 274
    Width = 75
    Height = 17
    Hint = 'First 8 characters of layer name'
    Caption = '{lyr}'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 20
    TabStop = False
    OnClick = BtnLyrClick
  end
  object Panel2: TPanel
    Left = 126
    Top = 76
    Width = 450
    Height = 219
    BevelOuter = bvNone
    TabOrder = 21
    Visible = False
    object edLbl1: TEdit
      Left = 25
      Top = 40
      Width = 385
      Height = 21
      MaxLength = 80
      TabOrder = 0
      OnEnter = edEnter
      OnExit = edExit
    end
    object edLbl2: TEdit
      Left = 25
      Top = 66
      Width = 385
      Height = 21
      MaxLength = 80
      TabOrder = 1
      OnEnter = edEnter
      OnExit = edExit
    end
    object edLbl3: TEdit
      Left = 25
      Top = 92
      Width = 385
      Height = 21
      MaxLength = 80
      TabOrder = 2
      OnEnter = edEnter
      OnExit = edExit
    end
    object edLbl4: TEdit
      Left = 25
      Top = 118
      Width = 385
      Height = 21
      MaxLength = 80
      TabOrder = 3
      OnEnter = edEnter
      OnExit = edExit
    end
    object edLbl5: TEdit
      Left = 25
      Top = 144
      Width = 385
      Height = 21
      MaxLength = 80
      TabOrder = 4
      OnEnter = edEnter
      OnExit = edExit
    end
  end
end
