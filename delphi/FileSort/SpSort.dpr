program SpSort;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils, System.Classes;

var
  FileName : string;
  Lines : TStringList;
  Splitted: TArray<String>;
  i : integer;
begin
  try
    FileName := ExtractFilePath(ParamStr(0)) + 'SpRpt.tmp';
    Lines := TStringList.Create();
    Lines.LoadFromFile(FileName);
    Lines.Sort;
    for i := 0 to Lines.Count - 1 do begin
      Splitted := Lines[i].Split([chr(9)]);
      if Length(Splitted) > 1 then
        Lines[i] := Splitted[Length(Splitted)-1]
      else
        Lines[i] := '';
    end;
    Lines.SaveToFile(FileName);
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
end.
