program ToClipbrd;

uses
//  Vcl.Forms,
  //Vcl.Dialogs,
  SysUtils,
  Clipbrd;

{$R *.res}

var
  Param1 : string;
  Action : string;   // indicates whether to append to current clipboard text.
                     //   'L'= append with a line feed,
                     //   'T'=append with a tab,
                     //   'F'=replace with file content (where remainder of parameter is a file name)
                     // anything else will replace existing clipboard text
  Replace : string;   // character that is to be replaced with double quote character
  f : textfile;
  str, cbstr : string;
  retryCount : integer;
  Success : boolean;
  MyException : Exception;
begin
//  Application.Initialize;
  Param1 := ParamStr(1);
  Action := Param1.Substring(0, 1);
  Replace := Param1.Substring(1, 1);
  Param1 := Param1.Substring(2);

  if param1.Length = 0 then
    Param1 := ParamStr(2);

  if Replace <> ' ' then
    param1 := stringreplace (param1, Replace, '"', [rfReplaceAll]);

  param1 := stringreplace (param1, string(chr(30)), sLineBreak, [rfReplaceAll]);

  if Action = 'L' then
    Clipboard.AsText := Clipboard.AsText + sLineBreak + Param1
  else if Action = 'T' then
    Clipboard.AsText := Clipboard.AsText + #9 + Param1
  else if Action = 'F' then begin
    AssignFile (f, Param1);
    reset (f);
    cbStr := '';
    repeat
      ReadLn (f, str);
      cbStr := cbStr + str;
      if not eof(f) then
        cbStr := cbStr + sLineBreak;
    until eof(f);
    CloseFile (f);
    DeleteFile (Param1);

    retryCount := 0;
    Success := false;
    while not Success do
    try
      Clipboard.AsText := cbStr;
      Success := True;
    except
      on Exception do
      begin
        Inc(RetryCount);
        if RetryCount < 5 then
          Sleep(RetryCount * 100)
        else
          raise MyException.Create('Cannot set clipboard');
      end;
    end;
  end
  else
    Clipboard.AsText := Param1;
end.
