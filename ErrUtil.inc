FUNCTION DisplayFileError (res : integer; desc : string) : boolean;			EXTERNAL;

PROCEDURE InitLog (MacroName : string);		EXTERNAL;

PROCEDURE LogProgress (str : str255); 	EXTERNAL;

PROCEDURE LogStr (str : string); 	EXTERNAL;

PROCEDURE LogStrStr (str1, str2 : string); 	EXTERNAL;

PROCEDURE LogStrRll (str : string; rll : real); 	EXTERNAL;

PROCEDURE LogStrDis (str : string; dis : real); 	EXTERNAL;

PROCEDURE LogStrAng (str : string; ang : real); 	EXTERNAL;

PROCEDURE LogStrInt (str : string; i : integer); 	EXTERNAL;

PROCEDURE LogStrBln (str : string; b : boolean); 	EXTERNAL;

PROCEDURE CloseLog; 	EXTERNAL;


