PROCEDURE SaveIniInt (name : string;  value : integer);		EXTERNAL;

PROCEDURE SaveInt (name : string;  value : integer; toini : boolean);		EXTERNAL;

FUNCTION GetIniInt (name : string; default : integer) : integer;		EXTERNAL;

FUNCTION GetSvdInt (name : string; default : integer) : integer;		EXTERNAL;

PROCEDURE InitSettings (iniName : string);		EXTERNAL;

PROCEDURE SaveAng (name : string;  value : real; toini : boolean);		EXTERNAL;

FUNCTION GetIniRl (name : string; default : real) : real;		EXTERNAL;

FUNCTION GetSvdAng (name : string; default : real) : real;		EXTERNAL;

PROCEDURE SaveRl (name : string;  value : real; toini : boolean);		EXTERNAL;

PROCEDURE SaveIniRl (name : string; value : real);	EXTERNAL;

FUNCTION GetSvdRl (name : string; default : real) : real;		EXTERNAL;

PROCEDURE SaveBln (name : string;  value : boolean; toini : boolean; toAttr : boolean);		EXTERNAL;

FUNCTION GetIniBln (name : string; default : boolean) : boolean;		EXTERNAL;

FUNCTION GetSvdBln (name : string; default : boolean) : boolean;		EXTERNAL;

PROCEDURE SaveIniStr (name : string;  value : string);		EXTERNAL;

PROCEDURE SaveStr (name : string;  value : string; toini : boolean);		EXTERNAL;

PROCEDURE GetIniStr (name : string; default : string; result : IN OUT string);		EXTERNAL;

PROCEDURE GetSvdStr (name : string; default : string; result : IN OUT string );		EXTERNAL;

PROCEDURE SaveLayer (name : string; lyr : IN OUT layer); EXTERNAL;

FUNCTION GetSvdLyr (name : string; lyr : OUT layer) : boolean; 	EXTERNAL;

PROCEDURE GetSvdValues1 (ReportColHeads : IN OUT array of str25; 
												 ReportData : IN OUT array of str35; 
												 ReportSubTots : IN OUT array of str35;
												 ReportTots : IN OUT array of str35; 
												 Styles : IN OUT array of TextStyle); EXTERNAL;

PROCEDURE GetSvdStrX (name : string; default : string; result : IN OUT string; MaxLen : integer); EXTERNAL;
