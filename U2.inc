PROCEDURE PlaceLabel (pline : IN OUT entity; 
											Label : IN OUT array of entity;
											numlines : integer;
											Styles : IN OUT array of TextStyle;
											LblsCntr : boolean;
											TxAng : real;
											autoplace : boolean;
											speclyr		: layer;
											associative : boolean;
											SepEnts : boolean); EXTERNAL;

PROCEDURE UpdateLabel (pline : IN OUT entity;
					   Label : IN array of entity;
					   numlines : integer);  EXTERNAL;

PROCEDURE GetProg (CADversion : IN OUT integer);	EXTERNAL;

FUNCTION SetCADVer : integer;	EXTERNAL;

PROCEDURE AddVoid (pnt : point); EXTERNAL;


!PROCEDURE ChangeEnt (ent : IN OUT entity);  EXTERNAL;

PROCEDURE HilitePln (ent : IN OUT entity; doRedraw : boolean);  EXTERNAL;

PROCEDURE Ent_Draw_On (ent : IN OUT entity;
					   drmode : integer); EXTERNAL;

PROCEDURE GetNextNumber (number: IN OUT str8); EXTERNAL;
PROCEDURE AutoNumberToggle (AutoNumber : IN OUT boolean; LinePat: array of str80; number : IN OUT str8);	EXTERNAL;