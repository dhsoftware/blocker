
FUNCTION CreateRptFile (fName : str255;
												mode : IN OUT mode_type;
												ReportColHeads : IN array of str25;
												ReportSubHead : IN str80;
												ReportData  : IN array of str35;
												ReportSubTots	: IN array of str35;
												ReportTots	: IN array of str35;
												Styles 			: IN array of TextStyle;
												Counts 			: IN OUT array of integer;
												NumCols			: IN OUT integer;
												ColWidths 	: IN OUT array of real;
												ColSpc : real;
												ByLyr, ByCat: boolean) : boolean;   EXTERNAL;
