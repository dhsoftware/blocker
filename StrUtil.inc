FUNCTION BlankString (s : string): boolean;		EXTERNAL;

FUNCTION ValidFileName (s : string; strname : string) : integer; 		EXTERNAL;

FUNCTION StartsWith (target : string; prefix : string; casesensative : boolean) : boolean;	EXTERNAL;

FUNCTION EndsWith (target : string; suffix : string; casesensative : boolean) : boolean;	EXTERNAL;

PROCEDURE AppendDateTimeStamp (Str : IN OUT String; 
															 doDate : boolean; 
															 doTime : boolean;
															 TimeColons : boolean;
															 spacebetween : boolean;
															 dohundredths : boolean);	EXTERNAL;
 
PROCEDURE RemoveChars (str : IN OUT string; CharsToRemove : string);	EXTERNAL;

PROCEDURE GetClrName (clr : IN integer; clrname : OUT string);	EXTERNAL;

PROCEDURE FractionalString (num : IN Real; str : IN OUT string; Denom : integer; RoundNear : boolean); EXTERNAL;

PROCEDURE DecimalString (num : IN Real; str : IN OUT string; SigDig : integer; RemTrail0 : boolean; RoundNear : boolean);  EXTERNAL;

PROCEDURE FeetInches (num : IN Real; str : IN OUT string; RoundNear : boolean); EXTERNAL;

PROCEDURE SafeStrCat (str1 : IN OUT string; str2 : string);	EXTERNAL;

PROCEDURE SafeStrIns (dest : IN OUT string; source : string; pos : integer); EXTERNAL;

FUNCTION Numeric (s : string; allowdecimal : boolean) : boolean; EXTERNAL;

PROCEDURE AreaString (area : real; areatype : integer; str_area : IN OUT string; str_units : IN OUT string);	EXTERNAL;

PROCEDURE DisString (dis : real; distype : integer; s_dis : IN OUT string; s_units : IN OUT string);	EXTERNAL;

PROCEDURE VolString (r_vol : real; voltype : integer; s_vol : IN OUT string; s_units : IN OUT string); EXTERNAL;

PROCEDURE ClipboardStr (s : string; directive : string; doMsg : boolean); EXTERNAL;

FUNCTION MyStrComp (str1, str2 : IN string; ignorecase : boolean) : integer; EXTERNAL;
