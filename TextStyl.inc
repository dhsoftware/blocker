TYPE
	TextStyle = RECORD
		TxClr : integer;
		TxSiz : real;
		TxSln : real;
		TxAsp : real;
		TxWgt : integer;
		Font	: str8;
		Space : real;
		Space1: real;
		Caps  : boolean;
		TxScale : boolean;
	END;