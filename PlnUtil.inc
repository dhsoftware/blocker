
PROCEDURE HidePln (ent : IN OUT entity; hideit : boolean); EXTERNAL;

PROCEDURE HidePlns (draw : boolean);		EXTERNAL;

PROCEDURE ShowPlns (draw : boolean);		EXTERNAL;

FUNCTION PlnFix (ent : IN OUT entity; isvoid : boolean) : boolean;	EXTERNAL;

FUNCTION plnAddVoid (ent, vent : IN OUT entity) : boolean;  EXTERNAL;

PROCEDURE PlnPerim (frst, last : IN pvtaddr; TotPerim : OUT real; VoidPerim : OUT real; NonVoidPerim : OUT real); EXTERNAL;

PROCEDURE MeasureEnt;  EXTERNAL;

PROCEDURE CreatePln (ent : IN OUT entity; doFill : boolean; CADVersion : integer);	EXTERNAL;

PROCEDURE PlnDimensions (pln : IN OUT entity; xdim, ydim : IN OUT real);	EXTERNAL;
