
PROCEDURE GetLabel (pline : IN OUT entity; Label : IN OUT array of entity; NumLines : IN OUT integer);  EXTERNAL;

FUNCTION GetPln (ent : IN OUT entity) : boolean;  EXTERNAL;

FUNCTION GetPlnTag (ent : IN OUT entity;
										tag : IN OUT atrname;
										stamp : IN OUT str80) : boolean;	EXTERNAL;

PROCEDURE AddAtr (ent : IN OUT entity;
									s_num : str8;
									s_name : str25;
									doFill : boolean;
									FillClr : integer ;
									TxtCntr : boolean); EXTERNAL;

FUNCTION AtrExists (ent : IN OUT entity) : boolean; EXTERNAL;

FUNCTION GetAtr (ent : IN OUT entity;
									s_num : IN OUT str8;
									s_name : IN OUT str25;
									doFill : OUT boolean;
									FillClr : OUT integer;
									TxtCntr : OUT boolean;
									area : OUT real;
									perim : OUT real;
									v_perim : OUT real;
									height : OUT real;
									centroid : OUT point;
									xdim, ydim : OUT real) : boolean;  EXTERNAL;

FUNCTION GetBasicAtr (ent : IN OUT entity;
											s_num : IN OUT str8;
											s_name : IN OUT str25;
											doFill : OUT boolean;
											FillClr : OUT integer;
											TxtCntr : OUT boolean) : boolean; EXTERNAL;

PROCEDURE CreateLblTag (label : IN OUT array of entity;
												plnent : IN OUT entity);	EXTERNAL;

FUNCTION LblCentered (ent : IN OUT entity) : boolean;  EXTERNAL;

PROCEDURE ChangeTag (pline : IN OUT entity;
										 Label : IN OUT array of entity;
										 NumLines : integer;
										 OldTag : atrname;
										 NewTag :IN OUT atrname);	EXTERNAL;