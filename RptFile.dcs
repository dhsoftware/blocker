MODULE RptFile;

# INCLUDE 'Types.inc'
# INCLUDE 'TxtStyle.inc'
# INCLUDE 'StrUtil.inc'
# INCLUDE 'Stg.inc'
# INCLUDE 'U2.inc'
# INCLUDE 'Links.inc'
# INCLUDE 'Lang.inc'
# INCLUDE 'ErrUtil.inc'
# INCLUDE 'SelSet.inc'
# INCLUDE 'SP.inc'
# INCLUDE 'PlnUtil.inc'
# INCLUDE 'Text.inc'
# INCLUDE '..\\..\\Inc\\_misc.inc'
# INCLUDE '..\\..\\Inc\\_pline.inc'
# INCLUDE '..\\..\\Inc\\_input.inc'

PROCEDURE readIniStr (IniFile, Section, Ident, Default : str255; ReturnStr : OUT str255); BUILTIN 625;
PROCEDURE writeIniStr (IniFile, Section, Ident, Value : str255); BUILTIN 624;
PROCEDURE execAndWait ( prog : str255; Visibility: integer; returnCode : OUT integer); BUILTIN 632;


FUNCTION MyDisplayFileError (res : integer; desc : string) : boolean;
BEGIN
	return DisplayFileError (res, desc);
END MyDisplayFileError;


FUNCTION DoGroups (ent : IN OUT entity; byLyr, byCat : boolean; GroupName, LyrName, CatName : IN OUT string) : boolean;
VAR
	atr : attrib;
BEGIN
	getlyrname (ent.lyr, lyrname);
	if atr_entfind (ent, 'dhSpPlnrCat', atr) then
		strassign (catname, atr.str);
	else
		catname := '';
	end;

	if ByLyr then
		strassign (GroupName, lyrname);
	else
		strassign (GroupName, catname);
	end;
	return byLyr or byCat;
END DoGroups;

PROCEDURE GetBlockerDtls (ent : IN OUT entity;  s_num : IN OUT str8; s_name : IN OUT str25); 
VAR
	atr : attrib;
BEGIN
	if atr_entfind (ent, '*BLNAME_TXT', atr) then
		if strlen(atr.str) > 25 then
			atr.str[0] := chr(25);
		end;
		strassign (s_name, atr.str);
	else
		s_name := '';
	end;
	if atr_entfind (ent, '*BLRMNO_TXT', atr) then
		if strlen(atr.str) > 8 then
			atr.str[0] := chr(8);
		end;
		strassign (s_num, atr.str);
	else
		s_num := '';
	end;
END GetBlockerDtls; 

FUNCTION SortedAdrFile (mode : IN OUT mode_type;
												Cols : IN array of str35;
												fltmp : IN OUT file;
												tempfname : str255;
												TotArea : real;
												ByLyr, ByCat, Blocker : boolean) : boolean;
VAR
	addr : entaddr;
	ent : entity;
	liaddr : longint;
	sortfname, fltxt : str255;
	s : string(16);
	bl, tr : point;
	r_area, r_parea, r_vol : real;
	s_area : str80;
	s_parea : str80;
	s_vol : str80;
	r_perim, v_perim, nv_perim : real;
	s_num : str8;
	s_name : str25;
	s_perim : str80;
	r_hdim, r_vdim : real;
	s_hdim, s_vdim : str80;
	i_percent : integer;
	s_percent : string(10);
	SQFTCONV, MCONV, M3CONV : real;
	tempbln : boolean;
	i, j, k : integer;
	atr : attrib;
	lyrname,
	catname : str80;
	r : real;
	p : point;
BEGIN
	SQFTCONV := 384.0*384.0;
	MCONV := (1000.0/25.4)*32.0;
	M3CONV := expt (MCONV, 3.0);
	if Blocker then
		j := 2;
	else
		j := 1;
	end;
	for k := 1 to j do
		if k = 2 then
			mode_atr (mode, '*BLNAME_TXT');	! include Blocker entities
		end;
		addr := ent_first (mode);
		while ent_get (ent, addr) do
			addr := ent_next (ent, mode);
		
			!Calculate perimeter
			PlnPerim (ent.plnFrst, ent.plnLast, r_perim, v_perim, nv_perim);
			cvrllst (r_perim/MCONV, s_perim);
			if strpos ('.', s_perim, 1) = 0 then
				strcat (s_perim, '.');
			end;
			while strpos ('.', s_perim, 1) < 10 do
				strins (s_perim, '0', 1);		! left pad with zeros to ensure string comparison is correct
			end;


			!Calculate dimensions
			ent_extent (ent, bl, tr);
			r_hdim := tr.x - bl.x;
			cvrllst (r_hdim/MCONV, s_hdim);
			if strpos ('.', s_hdim, 1) = 0 then
				strcat (s_hdim, '.');
			end;
			while strpos ('.', s_hdim, 1) < 10 do
				strins (s_hdim, '0', 1);		! left pad with zeros to ensure string comparison is correct
			end;

			r_vdim := tr.y - bl.y;
			cvrllst (r_vdim/MCONV, s_vdim);
			if strpos ('.', s_vdim, 1) = 0 then
				strcat (s_vdim, '.');
			end;
			while strpos ('.', s_vdim, 1) < 10 do
				strins (s_vdim, '0', 1);		! left pad with zeros to ensure string comparison is correct
			end;
			LogStr ('dim cal for sort file');

			! Calculate area & perimeter area
			r_area := absr (pline_area (ent.plnFrst, ent.plnLast));
			r_parea := r_perim * absr(ent.plnhite - ent.plnbase);
			r_vol := r_area * absr(ent.plnhite - ent.plnbase);
			LogStr ('sort file areas/volume');
			i_percent := round((r_area*30000.0)/TotArea);		! not interested in real figures, just relative sizes
			cvintst (i_percent, s_percent);
			while strlen(s_percent) < 9 do
				strins (s_percent, '0', 1);
			end;

			strassign (s_area, s_percent);		! not interested in real figures, just relative sizes. So just re-use percentage as area
			cvrllst (r_parea/SQFTCONV, s_parea);
			if strpos ('.', s_parea, 1) = 0 then
				strcat (s_parea, '.');
			end;
			while strpos ('.', s_parea, 1) < 10 do
				strins (s_parea, '0', 1);		! left pad with zeros to ensure string comparison is correct
			end;
			cvrllst (r_vol/M3CONV, s_vol);
			if strpos ('.', s_vol, 1) = 0 then
				strcat (s_vol, '.');
			end;
			while strpos ('.', s_vol, 1) < 10 do
				strins (s_vol, '0', 1);		! left pad with zeros to ensure string comparison is correct
			end;
			LogStr ('are/vol strings done');

			if k = 2 then	! Blocker entity
				GetBlockerDtls (ent, s_num, s_name); 
			else
				if not GetBasicAtr (ent ,s_num, s_name, tempbln, i, tempbln) then
					s_num := '';
					s_name := '';
				end;
			end;
			
			if DoGroups (ent, byLyr, byCat, fltxt, LyrName, CatName) then
				strcat (fltxt, '\t');
				if MyDisplayFileError (f_wrstr (fltmp,fltxt), 'Writing Group to Temp File') then
					beep;
					i := f_close (fltmp);
					return false;
				end;
			end;

			for i := 1 to 10 do
				if strlen (Cols[i]) > 0 then
					strassign (fltxt, Cols[i]);
					ReplaceTags (fltxt, s_num, s_name, s_area, s_hdim, s_vdim, s_perim, s_parea, s_vol, s_percent, lyrname, catname, '', r_hdim < r_vdim);
				else
					fltxt := ' ';
				end;
				strcat (fltxt, '\t');
				if MyDisplayFileError (f_wrstr (fltmp, fltxt), 'Writing temp Line to File') then
					beep;
					i := f_close (fltmp);
					return false;
				end;
			end;
			liaddr := longint(ent.addr);
			cvlntst (liaddr, fltxt);
			
			if MyDisplayFileError (f_wrstr (fltmp, fltxt), 'Writing addr to File') then
				beep;
				i := f_close (fltmp);
				return false;
			end;
			
			if MyDisplayFileError (f_wrln (fltmp), 'Writing LF to tmp file') then
				beep;
				i := f_close (fltmp);
				return false;
			end;
		end;
	end;

	if MyDisplayFileError (f_close (fltmp), 'Closing tmp file') then
		beep;
		return false;
	end;

	getpath (sortfname, pathsup);
	strcat (sortfname, 'dhsoftware\\SpSort.exe');
	ExecAndWait (sortfname, 0, i);
	if i <> 0 then
		beep;
		LogStrInt ('Exec error:', i);
		return false;
	end;
	return true;
END SortedAdrFile;

FUNCTION WriteFileLine (fl : IN OUT file; str : string; ErrMsg : string) : boolean;
VAR
	err : boolean;
	tempstr : str255;
	i : integer;
BEGIN
	if strlen (str) > 0 then
		err := MyDisplayFileError (f_wrstr (fl, str), str);
	else
		err := false;
	end;
	if not err then
		strassign (tempstr, str);
		strcat (tempstr, '(LF)');
		err := MyDisplayFileError (f_wrln (fl), tempstr);
	end;
	if err then
		beep;
		i := f_close (fl);
		return false;
	else
		return true;
	end;
END WriteFileLine;

FUNCTION OpenFiles (fname :str255; tempfname : IN OUT string; fltmp, fl : IN OUT file) : boolean;
VAR
	tempbln : boolean;

BEGIN
	if file_exist (fName) then
		tempbln := MyDisplayFileError (file_del (fName), 'Deleting rpt file');
	end;
	
	getpath (tempfname, pathsup);
	strcat (tempfname, 'dhsoftware\\SpRpt.tmp');
	if file_exist (tempfname) then
		tempbln := MyDisplayFileError (file_del (tempfname), 'Deleting temp file');
	end;
	if MyDisplayFileError (f_create (fltmp, tempfname, true), 'Creating temp file') then
		beep;
		return false;
	end;

	if MyDisplayFileError (f_create (fl, fName, true), 'Creating rpt file') then
		beep;
		return false;
	end;
	return true;
END OpenFiles;

PROCEDURE PercentStr (Area, TotArea : real; s_percent : IN OUT string);
VAR
	i_percent : integer;
BEGIN
	i_percent := round((Area*10000.0)/TotArea);
	cvintst (i_percent, s_percent);
	while strlen(s_percent) < 3 do
		strins (s_percent, '0', 1);
	end;
	strins (s_percent, '.', strlen(s_percent)-1);
END PercentStr;

FUNCTION DoSubHead (fl : IN OUT file;
										ReportSubHead : str80;
										Group : str80;
										Style : TextStyle;
										Count : IN OUT integer;
										SubHeadWidth : IN OUT real) : boolean;
VAR
	ent : entity;
	i : integer;
	bl, tr : point;
BEGIN
	ent_init (ent, enttxt);
	strassign (ent.txtstr, ReportSubHead);
	ReplaceTags (ent.txtstr, '', '', '', '', '', '', '', '', '', Group, Group, '', false);
	if strlen (ent.txtstr) > 0 then
		Count := Count + 1;
		FormatTxt (ent, Style);
		ent.txtAng := 0.0;
		ent_extent (ent, bl, tr);
		if SubHeadWidth < (tr.x - bl.x) then
			SubHeadWidth := tr.x - bl.x;
		end;
		if not WriteFileLine (fl, '10', 'Writing SubHead ndx to File') then
			beep;
			return false;
		end;
		if not WriteFileLine (fl, ent.txtstr, 'Writing SubHead to File') then
			beep; 
			 i := f_close (fl);
			 return false;
		end;
		for i := 2 to 10 do
			if not WriteFileLine (fl, '', 'Writing Filler to File') then
				beep; 
				 i := f_close (fl);
				 return false;
			end;
		end;
	end;
	return true;
END DoSubHead;


FUNCTION DoSubTots (fl : IN OUT file;
										ReportSubTots : Array of str35;
										GroupCount : IN OUT integer;
										NumCols : IN OUT integer;
										ColWidths : IN OUT array of real;
										ColSpc : real;
										Style : TextStyle;
										subTotPerim : real;
										DimDimTyp : integer;
										GroupTR, GroupBL : point;
										subTotArea : real;
										DimAreaTyp : integer;
										subTotPArea : real;
										DimpAreaTyp : integer;
										subTotVol : real;
										DimVolTyp : integer;
										TotArea : real;
										Group : str80) : boolean;
VAR
	s_perim, s_dunits,
	s_hdim, s_vdim, 
	s_area, s_areaunits,
	s_parea, s_pareaunits,
	s_vol, s_volunits,
	s_percent, s : str80;
	AvailColWidth : real;
	r_hdim, r_vdim : real;
	ent : entity;
	i : integer;
	bl, tr : point;
BEGIN
	DisString (subTotPerim, DimDimTyp, s_perim, s_dunits);
	r_hdim := Grouptr.x - Groupbl.x;
	r_vdim := Grouptr.y - Groupbl.y;
	DisString (r_hdim, DimDimTyp, s_hdim, s_dunits);
	DisString (r_vdim, DimDimTyp, s_vdim, s_dunits);
	AreaString (subTotArea, DimAreaTyp, s_area, s_areaunits);
	AreaString (subTotPArea, DimpAreaTyp+10, s_parea, s_pareaunits);
	VolString (subTotVol, DimVolTyp, s_vol, s_volunits);
	PercentStr (subTotArea, TotArea, s_percent);

	if not WriteFileLine (fl, '11', 'Writing SubTotLn ndx to File') then
		beep;
		return false;
	end;
	cvintst (GroupCount, s);
	AvailColWidth := 0.0;		! Allow first non-blank sub-total column to float back into previous columns,so keep a total of the previous col widths available.
	for i := 1 to 10 do
		if strlen (ReportSubTots[i]) > 0 then
			if i > NumCols then
				NumCols := i;
			end;

			ent_init (ent, enttxt);
			strassign (ent.txtstr, ReportSubTots[i]);
			ReplaceTags (ent.txtstr, '', '', s_area, s_hdim, s_vdim, s_perim, s_parea, s_vol, s_percent, Group, Group, s, r_hdim < r_vdim);
			FormatTxt (ent, Style);
			ent.txtAng := 0.0;
			ent_extent (ent, bl, tr);
			if AvailColWidth > 0.0 then
				if ColWidths[i] < (tr.x - bl.x - AvailColWidth) then
					ColWidths[i] := tr.x - bl.x - AvailColWidth;
					AvailColWidth := -2.0;
				end;
			else 
				if ColWidths[i] < tr.x - bl.x then
					ColWidths[i] := tr.x - bl.x;
				end;
			end;
		else
			ent.txtstr := '';
			if AvailColWidth > -1.0 then
				AvailColWidth := AvailColWidth + ColWidths[i] + ColSpc;
			end;
		end;
		if not WriteFileLine (fl, ent.txtstr, 'Writing SubTot Ln to File') then
			beep; 
			i := f_close (fl);
			return false;
		end;
	end;

	GroupCount := 0;
	return true;
END DoSubTots;

FUNCTION CreateRptFile (fName : str255;
												mode : IN OUT mode_type;
												ReportColHeads : IN array of str25;
												ReportSubHead : IN str80;
												ReportData  : IN array of str35;
												ReportSubTots	: IN array of str35;
												ReportTots	: IN array of str35;
												Styles 			: IN array of TextStyle;
												Counts 			: IN OUT array of integer;		!1=data lines, 2 = sub headings, 3 = sub totals
												NumCols			: IN OUT integer;
												ColWidths 	: IN OUT array of real;
												ColSpc : real;
												ByLyr, ByCat: boolean) : boolean;   PUBLIC;
VAR
	Blocker, tempbln : boolean;
	ent : entity;
	fl : file;
	fltmp : file;
	liaddr : longint;
	tempfname : str255;
	sortfname : str255;
	s : string(16);
	error : boolean;
	i, j : integer;
	bl, tr, groupbl, grouptr : point;
	TotXdim,
	TotYdim,
	TotArea, subTotArea,
	TotVol, subTotVol,
	TotPerim, subTotPerim,
	TotPArea , subTotPArea : real;
	r_area, r_parea, r_vol : real;
	s_area, s_areaunits : str80;
	s_parea, s_pareaunits : str80;
	s_vol, s_volunits : str80;
	r_perim, v_perim, nv_perim : real;
	s_num : str8;
	s_name : str25;
	s_perim, s_dunits : str80;
	r_hdim, r_vdim : real;
	s_hdim, s_vdim : str80;
	s_percent : str8;

	DimpAreaTyp,
  DimAreaTyp,
	DimDimTyp,
	DimVolTyp : integer;
  addr : entaddr;
	param : array [1..1] of str80;
	lyrname, catname,
	PrevGroup, GroupName : str80;
	GroupCount : integer;
	SubHeadWidth,
	AvailColWidth : real;
	first : boolean;
	r : real;
	p : point;
BEGIN
	if not OpenFiles (fname, tempfname, fltmp, fl) then
		beep;
		return false;
	end;

		if not WriteFileLine (fl, '7', 'Writing ColHead ndx to File') then
			beep;
			return false;
		end;

		NumCols := 0;
		for i := 1 to 10 do
			if strlen (ReportColHeads[i]) = 0 then
				ColWidths[i] := 0.0;
			else
				NumCols := i;
				ent_init (ent, enttxt);
				strassign (ent.txtstr, ReportColHeads[i]);
				FormatTxt (ent, Styles[7]);
				ent.txtAng := 0.0;
				ent_extent (ent, bl, tr);
				ColWidths[i] := tr.x - bl.x;
			end;
			if not WriteFileLine (fl, ReportColHeads[i], 'writing Hdg Line to file') then
				beep;
				return false;
			end;
		end;


	mode_enttype ( mode, entpln);
	mode_atr (mode, 'dhSPlannrPln');
	extents_mode ( mode, bl, tr);
	TotXdim := tr.x - bl.x;
	TotYdim := tr.y - bl.y;
	! need to get total area first so that we can calc area % if required
	TotArea := 0.0;
	TotPArea := 0.0;
	TotVol := 0.0;
	DimAreaTyp :=  GetSvdInt ('dhSPDimAraTp', 1);
	DimpAreaTyp := GetSvdInt ('dhSPPAreaTyp',1);
	DimDimTyp :=  GetSvdInt ('dhSPDimDimTp', 1);
	DimVolTyp :=  GetSvdInt ('dhSPDimVolTp', 1);
	
	for i := 1 to 3 do
		Counts[i] := 0;
	end;
	
	Blocker := getSvdBln ('dhSP_RptBlkr', false);
	if Blocker then
		j := 2;
	else
		j := 1;
	end;
	for i := 1 to j do
		if i = 2 then	! Add Blocker entites
			mode_atr (mode, '*BLNAME_TXT');
		end;
		addr := ent_first (mode);
		while ent_get (ent, addr) do
			ent_draw (ent, drmode_black);
			ent_draw (ent, drmode_white);
			addr := ent_next (ent, mode);
			Counts[1] := Counts[1] + 1;
			TotArea := TotArea + absr (pline_area (ent.PlnFrst, ent.PlnLast));
		end;
	end;
	mode_atr (mode, 'dhSPlannrPln');

	
	if counts[1] = 0 then
		beep;
		lwrterr (133);	!Nothing to report on
		i := f_close (fltmp);
		i := f_close (fl);
		return true;
	else
		cvintst (Counts[1], param[1]);
		lpwrterr (134, param, 1);		!Generating Report for $ Rooms. Please wait ...
	end;

	if not SortedAdrFile (mode, ReportData, fltmp, tempfname, TotArea, ByLyr, ByCat, Blocker) then
		beep;
		i := f_close (fltmp);
		i := f_close (fl);
		return false;
	end;

	
	if MyDisplayFileError (f_open (fltmp, tempfname, true, fmode_read), 'Re-opening tmp file') then
		beep;
		i := f_close (fltmp);
		i := file_del (tempfname);
		return false;
	end;
	
	logstr ('tmp file re-opened');

	if f_eof (fltmp) then
		beep;
		i := f_close (fltmp);
		i := file_del (tempfname);
		return false;
	end;
	
	TotPerim := 0.0;
	GroupCount := 0;
	SubHeadWidth := 0.0;
	PrevGroup := ' ';
	first := true;
	repeat
		if f_rdstr (fltmp, s) = fl_ok then
			LogStr ('tmp file read done');

			if cvstlnt (s, liaddr) then
				LogStr ('cvstlnt done');

				if ent_get (ent, entaddr(liaddr)) then
					getlyrname (ent.lyr, lyrname);
					if DoGroups (ent, byLyr, byCat, GroupName, LyrName, CatName) then
						if (GroupCount > 0) and not strcomp (GroupName, PrevGroup, -1) then
							if DoSubTots (fl, ReportSubTots, GroupCount, NumCols, ColWidths, ColSpc,
														Styles[10], subTotPerim, DimDimTyp, GroupTR, GroupBL,
														subTotArea, DimAreaTyp, subTotPArea, DimpAreaTyp, subTotVol,
														DimVolTyp, TotArea, PrevGroup) then
								Counts[3] := Counts[3] + 1;
							else
								beep; 
								i := f_close (fltmp);
								return false;
							end;
						elsif not first then
							GroupCount := GroupCount + 1;
						end;
						first := false;
						strassign (PrevGroup, GroupName);
						if GroupCount = 0 then
							subTotPArea := 0.0;
							subTotArea := 0.0;
							subTotVol := 0.0;
							subTotPerim := 0.0;
							GroupCount := 1;
						
							if not DoSubHead (fl, ReportSubHead, GroupName, Styles[10], Counts[2], SubHeadWidth) then 
								beep; 
								i := f_close (fltmp);
								return false;
							end;
						end;
						
						ent_extent (ent, bl, tr);
						if GroupCount = 1 then
							Groupbl := bl;
							Grouptr := tr;
						else
							if bl.x < Groupbl.x then
								Groupbl.x := bl.x;
							end;
							if bl.y < Groupbl.y then
								Groupbl.y := bl.y;
							end;
							if tr.x > Grouptr.x then
								Grouptr.x := tr.x;
							end;
							if tr.y > Grouptr.y then
								Grouptr.y := tr.y;
							end;
						end;
					end;
					!Calculate perimeter
					LogStr ('About to calculate perim2');
					PlnPerim (ent.plnFrst, ent.plnLast, r_perim, v_perim, nv_perim);
					TotPerim := TotPerim + r_perim;
					DisString (r_perim, DimDimTyp, s_perim, s_dunits);

					!Calculate dimensions
					ent_extent (ent, bl, tr);
					r_hdim := tr.x - bl.x;
					r_vdim := tr.y - bl.y;
					DisString (r_hdim, DimDimTyp, s_hdim, s_dunits);
					DisString (r_vdim, DimDimTyp, s_vdim, s_dunits);
					LogStr ('dimensions calculated2');

					! Calculate area & perimeter area
					r_area := absr (pline_area (ent.plnFrst, ent.plnLast));
					r_parea := r_perim * absr(ent.plnhite - ent.plnbase);
					TotPArea := TotPArea + r_parea;
					r_vol := r_area * absr(ent.plnhite - ent.plnbase);

					if ByLyr or ByCat then
						subTotVol := subTotVol+ r_vol;
						subTotPArea := subTotPArea + r_parea;
						subTotPerim := subTotPerim + r_perim;
						subTotArea := subTotArea + r_area;
					end;
					TotVol := TotVol + r_vol;
					LogStr ('areas and volume calculated2');
					PercentStr (r_Area, TotArea, s_percent);

					AreaString (r_area, DimAreaTyp, s_area, s_areaunits);
					LogStr ('first area string done2');
					AreaString (r_parea, DimpAreaTyp+10, s_parea, s_pareaunits);
					LogStr ('both areas strings done2');
					VolString (r_vol, DimVolTyp, s_vol, s_volunits);
					LogStr ('vol string done2');

					if not GetBasicAtr (ent ,s_num, s_name, tempbln, i, tempbln) then
						GetBlockerDtls (ent, s_num, s_name);
					end;
					if not WriteFileLine (fl, '8', 'Writing DataLn ndx to File') then
						beep;
						return false;
					end;
					for i := 1 to 10 do
						if strlen (ReportData[i]) > 0 then
							if i > NumCols then
								NumCols := i;
							end;

							ent_init (ent, enttxt);
							strassign (ent.txtstr, ReportData[i]);
							ReplaceTags (ent.txtstr, s_num, s_name, s_area, s_hdim, s_vdim, s_perim, s_parea, s_vol, s_percent, lyrname, catname, '', r_hdim < r_vdim);
							FormatTxt (ent, Styles[8]);
							ent.txtAng := 0.0;
							ent_extent (ent, bl, tr);
							if ColWidths[i] < tr.x - bl.x then
								ColWidths[i] := tr.x - bl.x;
							end;
						else
							ent.txtstr := '';
						end;
						if not WriteFileLine (fl, ent.txtstr, 'Writing Dtl Ln to File') then
							beep;
							 i := f_close (fltmp);
							 return false;
						end;
					end;
				end;
			end;
		end;

		if not f_eof(fltmp) then
			if MyDisplayFileError (f_rdln (fltmp), 'reading line tmp file') then
				beep;
				i := f_close (fltmp);
				i := f_close (fl);
				return false;
			end;
		end;

	until f_eof(fltmp);

	
	i := f_close (fltmp);
	i := file_del (tempfname);
	if MyDisplayFileError (i, 'deleting tmp file') then
	end;
	
	if (GroupCount > 0) then
		if DoSubTots (fl, ReportSubTots, GroupCount, NumCols, ColWidths, ColSpc,
									Styles[10], subTotPerim, DimDimTyp, GroupTR, GroupBL,
									subTotArea, DimAreaTyp, subTotPArea, DimpAreaTyp, subTotVol,
									DimVolTyp, TotArea, PrevGroup) then
			Counts[3] := Counts[3] + 1;
		else
			beep; 
			i := f_close (fltmp);
			return false;
		end;
	end;

	DisString (TotXdim, DimDimTyp, s_hdim, s_dunits);
	DisString (TotYdim, DimDimTyp, s_vdim, s_dunits);
	DisString (TotPerim, DimDimTyp, s_perim, s_dunits);
	VolString (TotVol, DimVolTyp, s_vol, s_volunits);

	AreaString (TotArea, DimAreaTyp, s_area, s_areaunits);
	AreaString (TotPArea, DimpAreaTyp+10, s_parea, s_pareaunits);

	if not WriteFileLine (fl, '9', 'writing tot ndx to file') then
		beep;
		return false;
	end;
	cvintst (Counts[1], s);
	
	for i := 1 to 10 do
		if strlen (ReportTots[i]) > 0 then
			if i > NumCols then
				NumCols := i;
			end;
			ent_init (ent, enttxt);
			strassign (ent.txtstr, ReportTots[i]);
			ReplaceTags (ent.txtstr, '', '', s_area, s_hdim, s_vdim, s_perim, s_parea, s_vol, '100.00', '', '', s, TotXdim < TotYdim);
			FormatTxt (ent, Styles[9]);
			ent.txtAng := 0.0;
			ent_extent (ent, bl, tr);
			if Colwidths[i] < tr.x - bl.x then
				ColWidths[i] := tr.x - bl.x;
			end;
		else
			ent.txtstr := '';
		end;
		if not WriteFileLine (fl, ent.txtstr, 'writing tots to file') then
			beep;
			return false;
		end;
	end;	
	
	if MyDisplayFileError (f_close (fl), 'closing file') then
		beep;
		return false;
	end;
	AvailColWidth := 0.0;
	for i := 1 to 10 do
		AvailColWidth := AvailColWidth + ColWidths[i];
	end;
	if AvailColWidth < SubHeadWidth then
		ColWidths[1] := ColWidths[1] + SubHeadWidth - AvailColWidth;
	end;
	
	return true;

END CreateRptFile;

END RptFile.