TYPE
	str50 = string(50);
	
FUNCTION ReportSettings (pnt : IN OUT point; key : IN OUT integer;
												 RptTitle : IN OUT str50;
												 ReportColHeads : IN OUT array of str25;
												 ReportSubHead : IN OUT str80;
												 ReportData  : IN OUT array of str35;
												 ReportSubTots  : IN OUT array of str35;
												 ReportTots	 : IN OUT array of str35) : integer;  EXTERNAL;

FUNCTION DoReports (pnt : IN OUT point; key : IN OUT integer; 
										RptTitle : IN str50;
										InputMode : integer;
										ReportColHeads : IN OUT array of str25;
										ReportSubHead : IN OUT str80;
										ReportData  : IN OUT array of str35;
										ReportSubTots : IN OUT array of str35;
										ReportTots	 : IN OUT array of str35;
										Styles : IN array of TextStyle) : integer; EXTERNAL;
